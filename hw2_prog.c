#include "hw2_routines.h"

int main()
{
  double a = 0.0;
  double b = 1.0;
  double result;
  double error;
  int n;
  
  printf("Midpoint rule\n");
  for (n=10; n<=100000; n*=10)
  {
    result = midpointRule(a, b, n); 
    printf("n = %d, error = %.15f\n", n, fabs(result-0.125));
  }
  
  printf("Trapezoid rule\n");
  for (n=10; n<=100000; n*=10)
  {
    result = trapezoidRule(a, b, n);
    printf("n = %d, error = %.15f\n", n, fabs(result-0.125));
  }
 
  printf("Simpson rule\n");
  for (n=10; n<=100000; n*=10)
  {
    result = simpsonRule(a, b, n); 
    printf("n = %d, error = %.30f\n", n, fabs(result-0.125));
  }
  return 0;
}
