# Files
EXEC := integrate
SRC  := $(wildcard *.c)
OBJ  := $(patsubst %.c,%.o,$(SRC))

# Options
CC      := icc
CFLAGS  := -O3
LDFLAGS := -L/lib
LDLIBS  := -lm

# Rules
$(EXEC): $(OBJ)
	$(CC) $(LDFLAGS) $(LDLIBS) -o $@ $^
%.o: %.c
	$(CC) $(CFLAGS) -c $<
hw2_prog.o hw2_routines.o: hw2_routines.h

#Add run target
run:integrate
	./integrate

# Useful phony targets
.PHONY: clobber clean neat echo
clobber: clean
	$(RM) $(EXEC)
clean: neat
	$(RM) $(OBJ)
neat:
	$(RM) *.o integrate
echo:
	@echo $(OBJ)
