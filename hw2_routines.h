#include <stdio.h>
#include <math.h>

double midpointRule(double, double, int);

double trapezoidRule(double, double, int);

double simpsonRule(double, double, int);
