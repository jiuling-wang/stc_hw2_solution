#include "hw2_routines.h"

double midpointRule(double x_start, double x_end, int n)
{
  int i = 0;
  double h = (x_start + x_end) / n;
  double x_i;  
  double i_h = 0.0;  
  
  for (i=0; i<n; i++)
  {
    x_i = x_start + i*h;
    i_h += pow(x_i+h/2.0, 7);
  }
  
  return h*i_h;

}

double trapezoidRule(double x_start, double x_end, int n)
{
  int i = 0;
  double h = (x_start + x_end) / n;
  double x_i, x_i1;
  double i_h = 0.0;

  for(i=0; i<n; i++)
  {
    x_i = x_start + i*h;
    x_i1 = x_start + (i+1)*h;
    i_h = i_h + pow(x_i, 7) + pow(x_i1, 7);
  }
 
  return (h/2.0)*i_h;
}


double simpsonRule(double x_start, double x_end, int n)
{
  int i = 0;
  double x_i0, x_i, x_i1;
  double h = (x_start + x_end) / n;
  double i_h = 0.0;

  for (i=1; i<n; i+=2)
  {
    x_i0 = x_start + (i-1)*h;
    x_i = x_start + i*h;
    x_i1 = x_start + (i+1)*h;    
    i_h = i_h + pow(x_i0, 7) + 4.0*pow(x_i, 7) + pow(x_i1, 7);
  }

  return (h/3.0)*i_h;
}
